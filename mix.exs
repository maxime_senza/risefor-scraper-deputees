defmodule ShopScraper.MixProject do
  use Mix.Project

  def project do
    [
      app: :shop_scraper,
      version: "0.1.0",
      elixir: "~> 1.5",
      elixirc_paths: elixirc_paths(Mix.env()),
      compilers: [:phoenix, :gettext] ++ Mix.compilers(),
      start_permanent: Mix.env() == :prod,
      aliases: aliases(),
      deps: deps()
    ]
  end

  # Configuration for the OTP application.
  #
  # Type `mix help compile.app` for more information.
  def application do
    [
      mod: {ShopScraper.Application, []},
      registered: [ShopScraper.Scraper.Supervisor, ShopScraper.Scraper.Registry, ShopScraper.Scraper.ShopRegistry],
      extra_applications: [:logger, :runtime_tools]
    ]
  end

  # Specifies which paths to compile per environment.
  defp elixirc_paths(:test), do: ["lib", "test/support"]
  defp elixirc_paths(_), do: ["lib"]

  # Specifies your project dependencies.
  #
  # Type `mix help deps` for examples and options.
  defp deps do
    [
      {:phoenix, "~> 1.4.2"},
      {:phoenix_pubsub, "~> 1.1"},
      {:phoenix_ecto, "~> 4.0"},
      {:ecto_sql, "~> 3.0"},
      {:postgrex, ">= 0.0.0"},
      {:phoenix_html, "~> 2.11"},
      {:phoenix_live_reload, "~> 1.2", only: :dev},
      {:gettext, "~> 0.11"},
      {:jason, "~> 1.0"},
      {:plug_cowboy, "~> 2.0"},
      # custom dependencies
      {:mandarin, "~> 0.1.0"},
      {:meeseeks, "~> 0.11.0"},
      {:httpoison, "~> 1.4"}
      # {:sweet_xml, "~> 0.6.6"}
      # {:poison, "~> 3.1"},
      # {:floki, "~> 0.20.0"},
      # {:cronex, github: "jbernardo95/cronex", ref: "master"},
      # {:absinthe_plug, "~> 1.4"},
      # {:edeliver, "~> 1.4.5"},
      # {:distillery, "~> 1.5", runtime: false},
      # {:remap, github: "ltrls/remap", ref: "master"},
      # {:cors_plug, "~> 1.5"},
      # {:ex_money, "~> 1.0"},
      # {:ex_admin, github: "smpallen99/ex_admin"}
    ]
  end

  # Aliases are shortcuts or tasks specific to the current project.
  # For example, to create, migrate and run the seeds file at once:
  #
  #     $ mix ecto.setup
  #
  # See the documentation for `Mix` for more info on aliases.
  defp aliases do
    [
      "ecto.setup": ["ecto.create", "ecto.migrate", "run priv/repo/seeds.exs"],
      "ecto.reset": ["ecto.drop", "ecto.setup"],
      test: ["ecto.create --quiet", "ecto.migrate", "test"]
    ]
  end
end
