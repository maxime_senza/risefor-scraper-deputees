# Define here the models for your scraped items
#
# See documentation in:
# https://docs.scrapy.org/en/latest/topics/items.html

import scrapy
#from representativeScraper.crawler import ShopScraper
from dataclasses import dataclass, field
from typing import Optional
from scrapy.item import Item, Field

from scrapy.loader import ItemLoader
from itemadapter import ItemAdapter
from itemloaders.processors import TakeFirst, MapCompose, Join,Identity

#import regex
import re

class Representative(scrapy.Item):
    scrap_id = scrapy.Field(input_processor=TakeFirst(),output_processor=TakeFirst())
    url = scrapy.Field(input_processor=TakeFirst(),output_processor=TakeFirst())
    function = scrapy.Field(input_processor=TakeFirst(),output_processor=TakeFirst())
    sexe = scrapy.Field(input_processor=TakeFirst(),output_processor=TakeFirst())
    nom = scrapy.Field(input_processor=TakeFirst(),output_processor=TakeFirst())
    prenom = scrapy.Field(input_processor=TakeFirst(),output_processor=TakeFirst())
    nom_complet = scrapy.Field(input_processor=TakeFirst(),output_processor=TakeFirst())
    photo = scrapy.Field(input_processor=TakeFirst(),output_processor=TakeFirst())
    profession = scrapy.Field(input_processor=TakeFirst(),output_processor=TakeFirst())
    groupe = scrapy.Field(input_processor=TakeFirst(),output_processor=TakeFirst())
    groupe_sigle = scrapy.Field(input_processor=TakeFirst(),output_processor=TakeFirst())
    nom_deptmt = scrapy.Field(input_processor=TakeFirst(),output_processor=TakeFirst())
    num_deptmt = scrapy.Field(input_processor=TakeFirst(),output_processor=TakeFirst())
    nom_region = scrapy.Field(input_processor=TakeFirst(),output_processor=TakeFirst())
    nom_circo = scrapy.Field(input_processor=TakeFirst(),output_processor=TakeFirst())
    num_circo = scrapy.Field(input_processor=TakeFirst(),output_processor=TakeFirst())
    pays = scrapy.Field(input_processor=TakeFirst(),output_processor=TakeFirst())
    commission_permanente = scrapy.Field(input_processor=TakeFirst(),output_processor=TakeFirst())
    commission_permanente_function = scrapy.Field(input_processor=TakeFirst(),output_processor=TakeFirst())
    groupe_etude = scrapy.Field(input_processor=Join(";"),output_processor=TakeFirst())
    missions_parlementaires = scrapy.Field(input_processor=Join(";"),output_processor=TakeFirst())
    autres_mandats = scrapy.Field(input_processor=Join(";"),output_processor=TakeFirst())

    adresse_permanence = scrapy.Field(input_processor=TakeFirst(),output_processor=TakeFirst())
    cp_permanence = scrapy.Field(input_processor=TakeFirst(),output_processor=TakeFirst())
    ville_permanence = scrapy.Field(input_processor=TakeFirst(),output_processor=TakeFirst())
    
    tel = scrapy.Field(input_processor=Identity(),output_processor=Join(";"))
    emails = scrapy.Field(input_processor=Identity(),output_processor=Join(";"))
    twitter = scrapy.Field(input_processor=Identity(),output_processor=Join(";"))
    facebook = scrapy.Field(input_processor=Identity(),output_processor=Join(";"))
    website = scrapy.Field(input_processor=Identity(),output_processor=Join(";"))


class candidatesCirco(scrapy.Item):
    url = scrapy.Field(input_processor=TakeFirst(),output_processor=TakeFirst())
    fullname = scrapy.Field(input_processor=TakeFirst(),output_processor=TakeFirst())
    position = scrapy.Field(input_processor=TakeFirst(),output_processor=TakeFirst())
    circo = scrapy.Field(input_processor=Join(),output_processor=Join())
    result = scrapy.Field(input_processor=TakeFirst(),output_processor=TakeFirst())
    