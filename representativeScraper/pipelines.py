# Define your item pipelines here
#
# Don't forget to add your pipeline to the ITEM_PIPELINES setting
# See: https://docs.scrapy.org/en/latest/topics/item-pipeline.html


# useful for handling different item types with a single interface
from itemadapter import ItemAdapter
import json
from scrapy.exporters import JsonItemExporter, CsvItemExporter
from scrapy.exceptions import DropItem

import datetime
import csv

import unicodedata

from representativeScraper.variables import *




class DuplicateUrls:
    def __init__(self):
        self.ids_seen = set()
    def process_item(self, item, spider):
        adapter = ItemAdapter(item)
        if (adapter['url']) or (adapter['product_url']) : 
            if adapter['url'] in self.ids_seen:
                raise DropItem(f"Duplicate item found: {item!r}")
            else:
                self.ids_seen.add(adapter['url'])
                return item
        else:
            print("url ")

class RepresentativeDataExporter_Csv(object):
    def __init__(self):
        print("data exporter",function)
        self.file = open("data/result/"+function+"-details.csv", 'wb')
        #self.file = open('dev/test.csv', 'wb')

        self.exporter = CsvItemExporter(self.file, str,delimiter='§')
        self.exporter.start_exporting()

    def close_spider(self, spider):
        self.exporter.finish_exporting()
        self.file.close()

    def process_item(self, item, spider):
        self.exporter.export_item(item)
        return item


class RepresentativeUrlExporter_Csv(object):
    def __init__(self):
        print("Url exporter")
        self.file = open('data/result/'+function+'-scraped-urls.csv', 'wb')
        #self.file = open('dev/test.csv', 'wb')
        self.exporter = CsvItemExporter(self.file, str)
        self.exporter.start_exporting()
        
    def close_spider(self, spider):
        self.exporter.finish_exporting()
        self.file.close()

    def process_item(self, item, spider):
        spiderName = spider.name
        self.exporter.export_item(item)
        return item
        
class candidateLegislativeExporter_Csv(object):
    def __init__(self):
        print("Url exporter")
        self.file = open('data/result/candidates-2nd-round.csv', 'wb')
        #self.file = open('dev/test.csv', 'wb')
        self.exporter = CsvItemExporter(self.file, str)
        self.exporter.start_exporting()
        
    def close_spider(self, spider):
        self.exporter.finish_exporting()
        self.file.close()

    def process_item(self, item, spider):
        spiderName = spider.name
        self.exporter.export_item(item)
        return item


 

