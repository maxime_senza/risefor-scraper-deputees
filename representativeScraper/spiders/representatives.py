#needed python modules
import csv
from urllib.parse import parse_qsl, urljoin, urlparse
import json
from random import randint
import re
from collections import OrderedDict
import warnings
import logging
from wsgiref.validate import validator

#scrappy imports
import scrapy
from scrapy.http import Request,FormRequest
from scrapy.utils.log import configure_logging 
from scrapy.loader import ItemLoader
from scrapy.utils.engine import get_engine_status
from scrapy.spiders import CrawlSpider, Rule
from scrapy.linkextractors import LinkExtractor



#project imports
from representativeScraper.items import *
from representativeScraper.variables import *



class Representative_Urls(scrapy.Spider):
    #get URLS of all product pages and export them to a csv 
    name = "representative_urls"

    #allowed_domains = ["cannabis-seeds-store.co.uk"]

    custom_settings = {
         'ITEM_PIPELINES': {
            'representativeScraper.pipelines.DuplicateUrls': 90,
            'representativeScraper.pipelines.RepresentativeUrlExporter_Csv':1000,
         }
    }
    
    all_representative_pages = []
    known_pagination = []

    def start_requests(self):
        #scrap listing pages
        # yield scrapy.Request('https://www.nossenateurs.fr/senateurs', self.getPagination)
        yield scrapy.Request('https://nosdeputes.fr/deputes', self.getPagination)
        
        
    def getPagination(self, response,known_pagination=known_pagination,all_representative_pages=all_representative_pages):
        #get next page & representative links
        next_pagination = response.xpath("//div[@class='pagination']//a/@href").get()
        representative_pages = response.xpath("//div[contains(@class,'list_dep')][not(contains(@class,'anciens'))]//span[@class='list_nom']/a/@href").extract()
        print("representative is",function)
        all_representative_pages.append(representative_pages)
        #print("all products here",all_representative_pages)
        index = 0
        
        #for each representative listing, get the product and yield an ID & url
        for representatives in all_representative_pages:
            for representative in representatives:
                url = response.urljoin(representative)
                print("new url is",url)
                index = index + 1
                yield{
                    'url' : url,
                    'scrap_id' : index,
                }
        #if the next page isn't known, fetch & parse it
        if (next_pagination not in known_pagination) and (next_pagination is not None ):
            known_pagination.append(next_pagination)
            print("next page is",response.urljoin(next_pagination))
            
            request = scrapy.Request(response.urljoin(next_pagination), callback=self.getPagination)
            yield request


class Representative_Details(scrapy.Spider):
    name = "representative-details"
    custom_settings = {
        'ITEM_PIPELINES': {
            'representativeScraper.pipelines.RepresentativeDataExporter_Csv':900,
            'representativeScraper.pipelines.DuplicateUrls': 1000,
            #'representativeScraper.pipelines.productVariationExporter_Csv':1000,
        }
    }

    
    #with open("./data/result/"+function+"-scraped-urls.csv",newline='\n') as csvfile:   
    with open("./data/result/"+function+"-scraped-urls.csv",newline='\n') as csvfile:   
        #create dictionary with all variable 
        scrapedProducts = csv.DictReader(csvfile)
        newRepresentativeDatabase = OrderedDict()
        index = 0

        for product in scrapedProducts: 
            index = index + 1
            #add each line of csv to our dictionary
            newRepresentativeDatabase[index] = product
            #print("product is",product)


    def start_requests(self,newRepresentativeDatabase=newRepresentativeDatabase):
        #print("new products are",newRepresentativeDatabase)
        index = 0
        for representative_id, representative_data in newRepresentativeDatabase.items():
            index = index +1
            for data in representative_data:
                #print("data is",representative_data,data,representative_data[data])                
                if 'scrap_id' in data:
                    scrap_id = representative_data[data]
                else:
                    scrap_id = index

                if 'url' in data:
                    url = representative_data[data]

                if (url is None) or ("URL" in url):
                    print("empty or title",data)
                    return
            yield scrapy.Request(
                url=url, 
                meta={'scrap_id': scrap_id},
                cookies={ 
                        'country': 'FR',
                        },
                dont_filter=True
                )           
    def parse(self, response):
        scrap_id = response.meta["scrap_id"]
        representative_url = response.request.url
         
        representative = ItemLoader(item=Representative(),response=response)
        #################
        # Get what we want from HTML
        #################
        #global info
        #productDescription = response.xpath("//div[@id='tab-description']/p/text()").getall()
        representative_name = response.xpath("//h1/text()").get()
        
        #civility
    
        #global info
        representative.add_value("scrap_id",scrap_id)
        representative.add_value("fullname",representative_name)
        representative.add_value("url",representative_url)
        representative.add_value("function",function)
        representative.add_xpath("photo","//div[@class='photo_senateur']/img/@src")
        representative.add_xpath("political_party","//h2[contains(text(),'Informations')]/following::ul/li[contains(text(),'Groupe politique')]/a/text()")
        representative.add_xpath("political_party_position","//h2[contains(text(),'Informations')]/following::ul/li[contains(text(),'Groupe politique')]/a/following::text()[1]")
        representative.add_xpath("profession","//h2[contains(text(),'Informations')]/following::ul/li[contains(text(),'Profession')]/a/text()")
        
        representative.add_xpath("twitter","//h2[contains(text(),'Informations')]/following::ul/li/a[contains(text(),'Twitter')]/@href") 
        representative.add_xpath("website","//h2[contains(text(),'Informations')]/following::ul/li/a[contains(text(),'Site web')]/@href") 

        #location info
        representative.add_xpath("election_department","//div[@class='info_senateur']/h2/a[2]/text()")

        #position info
        
        representative.add_xpath("permanentCommission","//h2[contains(text(),'Responsabilités')]/following::ul/li[contains(text(),'Commission permanente')]/ul/li//text()")
        representative.add_xpath("permanentCommissionFunction","//h2[contains(text(),'Responsabilités')]/following::ul/li[contains(text(),'Commission permanente')]/ul/li/text()[1]")
        
        representative.add_xpath("parliamentaryMissions","//h2[contains(text(),'Responsabilités')]/following::ul/li[contains(text(),'Missions parlementaires')]/ul/li//text()")
        representative.add_xpath("studyGroups","//h2[contains(text(),'Responsabilités')]/following::ul/li[contains(text(),'tudes et d')]/ul/li//text()")
        representative.add_xpath("extraFuctions","//h2[contains(text(),'Responsabilités')]/following::ul/li[contains(text(),'Fonctions judiciaires')]/ul/li//text()")
        
        print("result",response.xpath("//h2[contains(text(),'Responsabilités')]/following::ul/li[contains(text(),'tudes et d')]/ul/li/a/text()").getall())
        """
        address1
        address2
        postal_code
        city
        commune
        representative.add_value("country","France")
        
        election_region
        circonscription

        
        permanentCommissionFunction
        parliamentaryMissionsFunction
        
        studyGroupsFunction
        
        
        directEmail
        directPhones
        
        facebook
        website
        """
        yield representative.load_item()        


class Candidates_Round2_URLs(scrapy.Spider):
    #get URLS of all product pages and export them to a csv 
    name = "candidates-round2-urls"

    #allowed_domains = ["cannabis-seeds-store.co.uk"]

    custom_settings = {
         'ITEM_PIPELINES': {
            # 'representativeScraper.pipelines.DuplicateUrls': 90,
            'representativeScraper.pipelines.candidateLegislativeExporter_Csv':1000,
         }
    }
    
    all_representative_pages = []
    known_pagination = []

    def start_requests(self):
        #scrap listing pages
        # yield scrapy.Request('https://www.nossenateurs.fr/senateurs', self.getPagination)
        yield scrapy.Request('https://www.resultats-elections.interieur.gouv.fr/legislatives-2022/index.html', self.getPagination)
        
        
    def getPagination(self, response):
        #get next page & representative links
        departmentPages = response.xpath("//select[@id='listeDpt']/option/@value").extract()
        # print("list department pages",departmentPages)
        
        #for each department, fetch the page & get the list of circo's
        for departmentPage in departmentPages:
            if departmentPage != '#':
                yield scrapy.Request(response.urljoin(departmentPage), callback=self.getCirco)

        
    def getCirco(self, response):
        circoPages = response.xpath("//div[contains(@class,'pub-index-communes')]//h3[contains(text(),'Circonscriptions')]/following-sibling::p/a/@href").extract()
        # print("current page",circoPages,response.url)
        for circoPage in circoPages:
            #issue with exporting having 

            nombreDeCandidats = ['1','2']
            #the requestion to get both candidates must be here
            # if placed in next step, yeilding item stops the spider
            for numeroCandidat in nombreDeCandidats:
                print("numéro avant request",numeroCandidat)
                request = scrapy.Request(
                    response.urljoin(circoPage),
                    callback=self.getResults,
                    meta={'numeroCandidat': numeroCandidat},
                    dont_filter=True)
                yield request

    def getResults(self, response):
        numeroCandidat = response.meta["numeroCandidat"]
        print("page is",response.url,numeroCandidat)
        candidate = ItemLoader(item=candidatesCirco(),response=response)
        circo = response.xpath("//div[@class='span12 pub-fil-ariane']//a").extract()
        candidateTable = response.xpath("//table[contains(@class,'tableau-resultats-listes-ER')]//tbody")
        url = response.url
        circoFinalists = []
        # print("numéro is",numeroCandidat)
        candidatResult = candidateTable.xpath("//tr["+str(numeroCandidat)+"]/td[5]/text()").get()
        candidatNom = candidateTable.xpath("//tr["+str(numeroCandidat)+"]/td[1]/text()").get()
        candidatNom = candidatNom.replace("M. ","")
        candidatNom = candidatNom.replace("Mme ","")
        allinfo= {
            "url":url,
            "fullname":candidatNom,
            "position":numeroCandidat,
            "circo":circo,
            "result":candidatResult,
        }
        circoFinalists.append(allinfo)
        for finalist in circoFinalists: 
            print(finalist)
            candidate.add_value("url",finalist["url"])
            candidate.add_value("fullname",finalist["fullname"])
            candidate.add_value("position",finalist["position"])
            candidate.add_value("circo",finalist["circo"])
            candidate.add_value("result",finalist["result"])

            yield candidate.load_item()  
            


class nosDeputees_data(scrapy.Spider):
    #get URLS of all product pages and export them to a csv 
    name = "nosDeputees_deputees"

    #allowed_domains = ["cannabis-seeds-store.co.uk"]

    custom_settings = {
         'ITEM_PIPELINES': {
            'representativeScraper.pipelines.DuplicateUrls': 90,
            'representativeScraper.pipelines.RepresentativeDataExporter_Csv':1000,
         }
    }
    
    # all_representative_pages  = []
    known_pagination = []

    def start_requests(self):
        #scrap listing pages
        # yield scrapy.Request('https://www.nossenateurs.fr/senateurs', self.getPagination)
        yield scrapy.Request('https://nosdeputes.fr/deputes', self.getPagination)
        
        
    def getPagination(self, response,known_pagination=known_pagination):
        #get next page & representative links
        representative_pages = response.xpath("//div[@class='list_table']//td/a/@href").extract()
        # # print("representative is",representative_pages)
        # all_representative_pages.append(representative_pages)
        # print("all reps here",all_representative_pages)
        numeroIndex = 0
        
        #for each representative listing, get the product and yield an ID & url
        for representativeUrl in representative_pages:
            print(representativeUrl)
            url = response.urljoin(representativeUrl)
            print("new url is",url)
            numeroIndex +=  1
            request = scrapy.Request(
                response.urljoin(representativeUrl),
                callback=self.getRepInfo,
                meta={'id': numeroIndex},
                dont_filter=True,
            )
            yield request

    def getRepInfo(self,response):
        ###GET ACTIF OR NOT
        representativeStatus = response.xpath("//div[@class='info_depute']/h2/text()").extract()
        print("status",representativeStatus)
        for string in representativeStatus:
            if 'ancien' in string:
                print("status : ancien députée")
                raise Exception("ancien députée",response.url)
                return

        print("response is",response.url)
        representative = ItemLoader(item=Representative(),response=response)
        
        # #determine sexe
        # if 'm.' in fullname.lower():
        #     sexe = 'H'
        # elif 'mme' in fullname.lower():
        #     sexe = "F"
        # else:
        #     raise ValueError("No sexe")
        # representative.add_value("sexe",sexe)
        
        representative.add_value("url",response.request.url)
        representative.add_value("function","Député•e")


        representative.add_value("scrap_id",response.meta['id'])
        # representative.add_value("nom_complet",sexe)
        
        fullname = response.xpath("//h1/text()").get()
        representative.add_value("nom_complet",fullname)
        

        pictureURI = response.xpath("//div[@class='photo_depute']/img/@src").get()
        print("picture data is",pictureURI)
        representative.add_value("photo",response.urljoin(pictureURI))
        print(fullname)#,response.urljoin(pictureURI))


        #profession TODO
        
        # representative.add_xpath("profession","")

        representative.add_xpath("groupe","//div[@class='boite_depute']//li[contains(text(),'Groupe politique')]/a/text()")
        representative.add_xpath("groupe_sigle","//h1/following-sibling::h2/a[1]/text()")
        
        #groupe_sigle TODO

        #location info
        representative.add_xpath("nom_deptmt","//h1/following-sibling::h2/a[2]/text()")
        # representative.add_xpath("num_deptmt","")
        # representative.add_xpath("nom_region","")
        representative.add_xpath("nom_circo","//h1/following-sibling::h2/a[2]/text()")

        circo_num_search = response.xpath("//h1/following-sibling::h2/text()").extract()
        #num circo returns string as array, in there the num of the circo,
        #iterate & extract
        for string in circo_num_search:
            num = re.search("\d+",string,flags=re.IGNORECASE)
            if num:
                circo_num = num
                print(circo_num)
        representative.add_value("num_circo",circo_num)
        adresses_permanente = response.xpath("//li[contains(text(),'Par courr')]/ul/li/text()").getall()
        adresse_complete = ""
        for adresse in adresses_permanente:
            if adresse_complete == "":
                adresse_complete = adresse
            else:     
                adresse_complete = adresse_complete + ". " + adresse
        # print("address is",adresse_complete)
        representative.add_value("adresse_permanence",adresse_complete)
        # representative.add_xpath("cp_permanence","")
        # representative.add_xpath("ville_permanence","")
        representative.add_value("pays","France")

        # representative.add_xpath("political_party_position","//h2[contains(text(),'Informations')]/following::ul/li[contains(text(),'Groupe politique')]/a/following::text()[1]")
        representative.add_xpath("profession","//li[contains(text(),'Profession')]/a/text()")

        #position info
        representative.add_xpath("commission_permanente","//li[contains(text(),'Commission permanente')]/ul/li/a/text()")
        commissionFunction = response.xpath("//li[contains(text(),'Commission permanente')]/ul/li/text()").get()
        commissionFunction = commissionFunction.strip().replace("(","").replace(")","")
        representative.add_value("commission_permanente_function",commissionFunction)
        representative.add_xpath("missions_parlementaires","//li[contains(text(),'Missions parlementaires')]/ul/li/a/text()")
        # representative.add_xpath("groupe_etude","")
        # representative.add_xpath("autres_mandats","")
        
        

        #Contact & website
        #tel TODO
        representative.add_xpath("emails","//li[contains(text(),'Par e-mail')]/ul/li/a/text()")
        twitterAccount = response.xpath("//a[contains(text(),'Compte Twitter')]/@href").get()
        twitterAccount = twitterAccount.replace("https://twitter.com/","")
        print(twitterAccount)
        representative.add_xpath("twitter",twitterAccount)

        representative.add_xpath("facebook","//a[contains(text(),'Page Facebook')]/@href")
        representative.add_xpath("website","//a[contains(text(),'Site web')]/@href") 
        
                

        yield representative.load_item()    


        

class assembleeNational_data(scrapy.Spider):
    #get URLS of all product pages and export them to a csv 
    name = "assembleeNational_deputees"

    #allowed_domains = ["cannabis-seeds-store.co.uk"]

    custom_settings = {
         'ITEM_PIPELINES': {
            'representativeScraper.pipelines.DuplicateUrls': 90,
            'representativeScraper.pipelines.RepresentativeDataExporter_Csv':1000,
         }
    }
    
    # all_representative_pages  = []
    known_pagination = []

    def start_requests(self):
        #scrap listing pages
        # yield scrapy.Request('https://www.nossenateurs.fr/senateurs', self.getPagination)
        yield scrapy.Request('https://www2.assemblee-nationale.fr/deputes/liste/alphabetique', self.getPagination)
        
        
    def getPagination(self, response,known_pagination=known_pagination):
        #get next page & representative links
        representative_pages = response.xpath("//div[@id='deputes-list']//li/a/@href").extract()
        # # print("representative is",representative_pages)
        # all_representative_pages.append(representative_pages)
        # print("all reps here",all_representative_pages)
        numeroIndex = 0
        
        #for each representative listing, get the product and yield an ID & url
        for representativeUrl in representative_pages:
            print(representativeUrl)
            url = response.urljoin(representativeUrl)
            print("new url is",url)
            numeroIndex +=  1
            request = scrapy.Request(
                response.urljoin(representativeUrl),
                callback=self.getRepInfo,
                meta={'id': numeroIndex},
                dont_filter=True,
            )
            yield request

    def getRepInfo(self,response):
        print("response is",response.url)
        fullname = response.xpath("//h1/text()").get()
        representative = ItemLoader(item=Representative(),response=response)
        
        #determine sexe
        if 'm.' in fullname.lower():
            sexe = 'H'
        elif 'mme' in fullname.lower():
            sexe = "F"
        else:
            raise ValueError("No sexe")
        representative.add_value("sexe",sexe)
        
        representative.add_value("url",response.request.url)
        representative.add_value("function","Député•e")


        representative.add_value("scrap_id",response.meta['id'])
        fullname = fullname.replace("M. ","")
        fullname = fullname.replace("Mme ","")
        representative.add_value("nom_complet",fullname)
        representative.add_value("url",response.url)
        

        print(fullname,sexe)
        representative.add_xpath("photo","//div[@class='deputes-image']/img/@src")

        #profession TODO

        representative.add_xpath("groupe","//div[@class='deputes-image']/following-sibling::span/a/text()")
        
        #groupe_sigle TODO

        #location info
        representative.add_xpath("nom_deptmt","//div[@class='info_senateur']/h2/a[2]/text()")
        # num_deptmt
        # nom_region
        # nom_circo
        # num_circo
        # adresse_permanence
        # cp_permanence
        # ville_permanence
        representative.add_value("pays","France")

        # representative.add_xpath("political_party_position","//h2[contains(text(),'Informations')]/following::ul/li[contains(text(),'Groupe politique')]/a/following::text()[1]")
        # representative.add_xpath("profession","//h2[contains(text(),'Informations')]/following::ul/li[contains(text(),'Profession')]/a/text()")


        #Contact & website
        #tel TODO
        representative.add_xpath("email","//h3[contains(text(),'Mél et site internet')]/following-sibling::dd[contains(text(),'Adresse électronique')]/a/text()") 
        representative.add_xpath("email","//h3[contains(text(),'Mél et site internet')]/following-sibling::dd[contains(text(),'Autre mél')]/a/text()") 
        representative.add_xpath("twitter","//h3[contains(text(),'Réseaux sociaux')]/following-sibling::dd/a[contains(@class,'twitter')]/@href")
        representative.add_xpath("facebook","//h3[contains(text(),'Réseaux sociaux')]/following-sibling::dd/a[contains(@class,'facebook')]/@href")
        representative.add_xpath("website","//h3[contains(text(),'Mél et site internet')]/following-sibling::dd[contains(text(),'Site internet')]/a/text()") 
        
                



        #position info
        
        representative.add_xpath("commission_permanente","//h2[contains(text(),'Responsabilités')]/following::ul/li[contains(text(),'Commission permanente')]/ul/li//text()")
        representative.add_xpath("commission_permanente_function","//h2[contains(text(),'Responsabilités')]/following::ul/li[contains(text(),'Commission permanente')]/ul/li/text()[1]")
        
        representative.add_xpath("missions_parlementaires","//h2[contains(text(),'Responsabilités')]/following::ul/li[contains(text(),'Missions parlementaires')]/ul/li//text()")
        representative.add_xpath("groupe_etude","//h2[contains(text(),'Responsabilités')]/following::ul/li[contains(text(),'tudes et d')]/ul/li//text()")
        representative.add_xpath("autres_mandats","//h2[contains(text(),'Responsabilités')]/following::ul/li[contains(text(),'Fonctions judiciaires')]/ul/li//text()")
        

        yield representative.load_item()        
