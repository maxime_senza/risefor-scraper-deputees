defmodule ShopScraper.Woocommerce.ProductVariation do
  import ShopScraper.Woocommerce

  alias ShopScraper.Admin.Shop
  alias ShopScraper.Admin.Product
  alias ShopScraper.Admin.Price

  def create_prices(%Shop{} = shop) do
    ShopScraper.Repo.preload(shop, :prices)
    |> Map.get(:prices)
    |> Enum.each(fn (price) -> create_or_update_price(price) end)
  end
  def create_prices(%Product{} = product) do
    ShopScraper.Repo.preload(product, :prices)
    |> Map.get(:prices)
    |> Enum.each(fn (price) -> create_or_update_price(price) end)
  end
  def create_prices() do
    ShopScraper.Admin.list_prices([])
    # |> Enum.take(1)
    |> Enum.each(fn (price) -> create_or_update_price(price) end)
  end

  def create_or_update_price(%Price{shop: %Ecto.Association.NotLoaded{}} = price) do
    ShopScraper.Repo.preload(price, :shop)
    |> create_or_update_price()
  end
  def create_or_update_price(%Price{product: %Ecto.Association.NotLoaded{}} = price) do
    ShopScraper.Repo.preload(price, :product)
    |> create_or_update_price()
  end
  def create_or_update_price(%Price{woocommerce_id: nil} = price) do
    create_price(price)
  end
  def create_or_update_price(%Price{} = price) do
    update_price(price)
  end

  def create_price(%Price{} = price) do
    {:ok, price_usd} = Money.to_currency(price.amount, :USD)
    price_map = %{
      "regular_price" => Money.to_decimal(price_usd),
      "virtual" => true,
      "attributes" => [
        %{"id" => 9,
          "option" => to_string(price.quantity) },
        %{"id" => 10,
          "option" => price.shop.name }
      ]
    }
    %{"id" => woocommerce_id} =
      post_create_variation(price_map, price.product.woocommerce_id)
      |> Jason.decode!()
    ShopScraper.Admin.update_price_woocommerce_id(price, woocommerce_id)
  end

  def update_price(%Price{} = price) do
    {:ok, price_usd} = Money.to_currency(price.amount, :USD)
    price_map = %{
      "regular_price" => Money.to_decimal(price_usd),
      "description" => URI.encode(price.url)
    }
    %{"id" => _woocommerce_id} =
      post_update_variation(price_map, price.product.woocommerce_id, price.woocommerce_id)
      |> Jason.decode!()
  end

  def list_product_variations(product_id) do
    get("/wp-json/wc/v2/products/#{product_id}/variations")
  end

  defp post_create_variation(params, product_id) do
    post("/wp-json/wc/v2/products/#{product_id}/variations", params)
  end

  defp post_update_variation(params, product_id, variation_id) do
    post("/wp-json/wc/v2/products/#{product_id}/variations/#{variation_id}", params)
  end

  def delete_product_variation(product_id, variation_id) do
    delete("/wp-json/wc/v2/products/#{product_id}/variations/#{variation_id}")
  end
end
