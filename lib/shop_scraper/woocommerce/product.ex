defmodule ShopScraper.Woocommerce.Product do
  import ShopScraper.Woocommerce

  alias ShopScraper.Admin
  alias ShopScraper.Admin.Product
  alias ShopScraper.Woocommerce.Attribute
  alias ShopScraper.Woocommerce.ProductVariation
  alias ShopScraper.Repo

  def fetch_all_products() do
    list_products()
    |> Enum.map(fn (product) ->
      name_params =
        %{name: product["name"],
          woocommerce_id: product["id"]
          # prices: prices,
          # attribute_values: attr_values
        }
      with {:ok, saved_product} <- ShopScraper.Admin.create_product(name_params) do
        attr_values =
          product["attributes"]
          |> Enum.filter(&(&1["variation"] == false))
          |> Enum.map(fn (attr) ->
            %{attribute: Admin.get_attribute_by_woocommerce_id(attr["id"]),
              value: List.first(attr["options"]),
              product: saved_product
            } |> ShopScraper.Admin.create_attribute_value()
          end)
        prices =
          ProductVariation.list_product_variations(product["id"])
          |> Enum.map(fn (variation) ->
            url = case Regex.run(~r/<p>(http.*)<\/p>/, variation["description"], capture: :all_but_first) do
              nil -> variation["description"]
              [match] -> match
            end
            %{amount: variation["price"],
              quantity: Enum.find(variation["attributes"], &(&1["name"] == "Number of Seeds"))["option"],
              shop: Enum.find(variation["attributes"], &(&1["name"] == "Store"))["option"] |> Attribute.normalize_shop_name() |> Admin.get_shop_by_name(),
              url: url,
              woocommerce_id: variation["id"],
              product: saved_product
            } |> ShopScraper.Admin.create_price()
          end)
       %{saved_product | attribute_values: attr_values, prices: prices}
      end
    end)
  end

  def create_or_update_product(%Product{woocommerce_id: nil} = product) do
    create_product(product)
  end
  def create_or_update_product(%Product{} = product) do
    update_product(product)
  end

  def create_products() do
    ShopScraper.Admin.list_products([])
    |> Enum.each(fn (product) -> create_or_update_product(product) end)
  end

  def update_products_name() do
    ShopScraper.Admin.list_products([])
    # |> Enum.take(1)
    |> Enum.each(fn (product) -> update_product(product) end)
  end

  def get_product(product) do
    get_one_product(product.woocommerce_id)
    |> Jason.decode!
    |> IO.inspect()
  end

  def attributes_list do
    [
      %{"id" => 25, "visible" => true},  # Product Name
      %{"id" => 24, "visible" => true},  # Breader / bank
      %{"id" => 12, "visible" => false}, # Cheapest Store,
      %{"id" => 11, "visible" => false}, # Cheapest (1) Seed 
      %{"id" => 7,  "visible" => true},  # Phenotype (Sativa/Indica)
      %{"id" => 18, "visible" => true},  # Seed Type
      %{"id" => 10,                      # Store
        "options" => ["Sensi Seeds", "I Love Growing Marijuana"],
        "visible" => false,
        "variation" => true
      },
      %{"id" => 19, "visible" => true},  # Lineage
      %{"id" => 14, "visible" => true},  # Flowering Length (Days)
      %{"id" => 13, "visible" => false}, # Flowering Length
      %{"id" => 22, "visible" => true},  # Plant Characteristics
      %{"id" => 21, "visible" => true},  # Climat Resistance
      %{"id" => 15, "visible" => true},  # Outdoor Climat
      %{"id" => 16, "visible" => true},  # Product Awards
      %{"id" => 23, "visible" => true},  # Grower Difficulty
      %{"id" => 5,  "visible" => true},  # High/Effects
      %{"id" => 20, "visible" => true},  # Medical Usage
      %{"id" => 26, "visible" => true},  # Possible Side Effects
      %{"id" => 2,  "visible" => true},  # Taste/Flavor
      %{"id" => 6,  "visible" => true},  # Odor
      %{"id" => 3,  "visible" => true},  # THC Levels
      %{"id" => 4,  "visible" => true},  # CBD Levels
      %{"id" => 27, "visible" => true},  # THC vs CBD
      %{"id" => 17, "visible" => true},  # Yield
      %{"id" => 9,                       # Number of seeds
        "options" => ["1", "3", "5", "10", "20", "25"],
        "visible" => false,
        "variation" => true
      }
    ]
    |> Enum.with_index()
    |> Enum.map(fn {el, i} -> Map.put(el, "position", i) end)
  end

  def product_params(%Product{} = product, options \\ %{}) do
    %{
      "name" => product.name,
      # "regular_price" => ShopScraper.Admin.minimum_price(product),
      "type" => "variable"
    }
    |> Map.merge(options)
  end

  def create_product(%Product{} = product) do
    options = %{"status" => "draft",
                "attributes" => attributes_list()}
    product_map = product_params(product, options)
    %{"id" => woocommerce_id} =
      post_create_product(product_map)
      |> Jason.decode!()
    ShopScraper.Admin.update_product(product, %{"woocommerce_id" => woocommerce_id})
  end

  def update_product(%Product{} = product) do
    product_map = product_params(product)
    %{"id" => _woocommerce_id} =
      post_update_product(product_map, product.woocommerce_id)
      |> Jason.decode!()
  end

  def list_products() do
    get_depaginated("/wp-json/wc/v2/products")
  end

  def get_one_product(product_id) do
    get("/wp-json/wc/v2/products/#{product_id}")
  end

  defp post_create_product(params) do
    post("/wp-json/wc/v2/products", params)
  end

  def post_update_product(params, product_id) do
    post("/wp-json/wc/v2/products/#{product_id}", params)
  end
end
