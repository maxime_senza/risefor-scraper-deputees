defmodule ShopScraper.Woocommerce.Attribute do
  require Logger

  import ShopScraper.Woocommerce

  alias ShopScraper.Admin.Attribute

  def fetch_all_attributes() do
    list_attributes()
    |> Enum.map(fn
      (%{"name" => "Store", "id" => id}) ->
        list_attribute_terms(id)
        |> IO.inspect()
        |> Enum.map(fn (%{"name" => name}) ->
          %{name: normalize_shop_name(name),
          scraper_params: %{}}
          |> ShopScraper.Admin.create_shop()
        end)
      (%{"name" => "Number of Seeds"}) ->
        nil
      (attr) ->
      %{name: attr["name"],
        woocommerce_id: attr["id"],
        params: %{}}
      |> ShopScraper.Admin.create_attribute()
    end)
  end

  def normalize_shop_name(name) do
    case Regex.run(~r/([\w ]+) \(.*\)/, name, capture: :all_but_first) do
      nil -> name
      [match] -> match
    end
  end

  def list_attributes() do
    get("/wp-json/wc/v2/products/attributes")
  end

  def list_attribute_terms(id) do
    get_depaginated("/wp-json/wc/v2/products/attributes/#{id}/terms")
  end
end
