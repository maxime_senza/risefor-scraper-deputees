defmodule ShopScraper.Woocommerce.LegacyFixStuff do
  require Ecto.Query

  import ShopScraper.Woocommerce
  import ShopScraper.Woocommerce.Product
  import ShopScraper.Woocommerce.ProductVariation

  alias ShopScraper.Admin.Product

  def fix_products_attributes() do
    ShopScraper.Admin.list_products([])
    # |> Enum.take(1)
    |> Enum.each(fn (product) -> fix_product_attributes(product) end)
  end

  def fix_products_old_variations() do
    ShopScraper.Admin.list_products([])
    # |> Enum.take(1)
    |> Enum.each(fn (product) -> fix_product_old_variation(product) end)
  end

  def fix_product_attributes(%Product{} = product) do
    %{"attributes" => attr_map} =
      get_one_product(product.woocommerce_id)
      |> Jason.decode!
    mandatory_attr = [
        %{"id" => 10,
          "options" => ["Sensi Seeds", "I Love Growing Marijuana"],
          "visible" => false,
          "variation" => true
        },
        %{"id" => 9,
          "options" => ["1", "3", "5", "10", "20", "25"],
          "visible" => false,
          "variation" => true
        }#,
        # %{"id" => 12,
        #   "options" => ["Sensi Seeds"],
        #   "visible" => true,
        #   "variation" => false
        # },
        # %{"id" => 11,
        #   "options" => ["1"],
        #   "visible" => true,
        #   "variation" => false
        # }
      ]
    attr_map = attr_map
      |> Enum.filter(fn %{"id" => id} -> id not in [9, 10, 11, 12] end)
      |> Kernel.++(mandatory_attr)
      |> Enum.with_index()
      |> Enum.map(fn {el, i} -> Map.put(el, "position", i) end)

    product_map = %{
      "attributes" => attr_map 
    }
    %{"id" => _woocommerce_id} =
      post_update_product(product_map, product.woocommerce_id)
      |> Jason.decode!()
  end

  def fix_product_old_variation(%Product{} = product) do
    variations_list =
      list_product_variations(product.woocommerce_id)
      |> Jason.decode!

    variations_list
    |> Enum.map(fn %{"id" => id} ->
      price = Ecto.Query.from(p in ShopScraper.Admin.Price, where: p.woocommerce_id == ^id) |> ShopScraper.Repo.one
      unless (price), do: delete_product_variation(product.woocommerce_id, id)
    end)
  end
end
