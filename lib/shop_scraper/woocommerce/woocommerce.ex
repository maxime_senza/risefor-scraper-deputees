defmodule ShopScraper.Woocommerce do
  require Logger

  def get_depaginated(url, params \\ []) do
    get_depaginated(url, 1, params)
  end
  def get_depaginated(url, page, params) do
    case get(url, [{:per_page, 100}, {:page, page} | params]) do
      [] -> []
      results -> results ++ get_depaginated(url, page + 1, params)
    end
  end

  def get(url, params \\ []) do
    headers = [{"Accept", "application/json"},
      {"Accept", "application/json; charset=UTF-8"},
      {"Content-Type", "application/json; charset=UTF-8"}]
    config = Application.get_env(:shop_scraper, ShopScraper.Woocommerce)

    # full_url = URI.merge(Keyword.get(config, :host), url) |> to_string()
    hackney = [basic_auth: {Keyword.get(config, :key), Keyword.get(config, :secret)}]
    # necessary as long as we Authorization headers are stripped
    params = [{:consumer_key, Keyword.get(config, :key)},
              {:consumer_secret, Keyword.get(config, :secret)} | params]
              # full_url = "#{full_url}?consumer_key=#{Keyword.get(config, :key)}&consumer_secret=#{Keyword.get(config, :secret)}"
    host = URI.parse(Keyword.get(config, :host))
    full_url = URI.to_string(%{host | path: url, query: URI.encode_query(params)}) 
    Logger.debug("GET #{full_url}")

    case HTTPoison.get(full_url, headers, timeout: 50_000, recv_timeout: 50_000, ssl: [{:versions, [:"tlsv1.2"]}], hackney: hackney) do
      {:ok, %HTTPoison.Response{status_code: 200, body: body}} ->
        Jason.decode!(body)

      {:ok, %HTTPoison.Response{status_code: 201, body: body}} ->
        Jason.decode!(body)

      {:ok, %HTTPoison.Response{status_code: 404}} ->
        IO.puts("Not found :(")

      {:error, %HTTPoison.Error{reason: reason}} ->
        IO.inspect(reason)
    end
  end

  def post(url, params) do
    raise "not ready yet"
    Logger.debug(inspect(params))
    headers = [{"Accept", "application/json"},
      {"Accept", "application/json; charset=UTF-8"},
      {"Content-Type", "application/json; charset=UTF-8"}]
    config = Application.get_env(:shop_scraper, ShopScraper.Woocommerce)

    full_url = URI.merge(Keyword.get(config, :host), url) |> to_string()
    hackney = [basic_auth: {Keyword.get(config, :key), Keyword.get(config, :secret)}]

    Logger.debug("POST #{full_url} with params #{inspect(params)}")

    case HTTPoison.post(full_url, Jason.encode!(params), headers, timeout: 50_000, recv_timeout: 50_000, ssl: [{:versions, [:"tlsv1.2"]}], hackney: hackney) do
      {:ok, %HTTPoison.Response{status_code: 200, body: body}} ->
        body

      {:ok, %HTTPoison.Response{status_code: 201, body: body}} ->
        body

      {:ok, %HTTPoison.Response{status_code: 404}} ->
        IO.puts("Not found :(")

      {:error, %HTTPoison.Error{reason: reason}} ->
        IO.inspect(reason)
    end
  end

  def delete(url) do
    raise "not ready yet"
    headers = [{"Accept", "application/json"},
      {"Accept", "application/json; charset=UTF-8"},
      {"Content-Type", "application/json; charset=UTF-8"}]
    config = Application.get_env(:shop_scraper, ShopScraper.Woocommerce)

    full_url = URI.merge(Keyword.get(config, :host), url) |> to_string()
    hackney = [basic_auth: {Keyword.get(config, :key), Keyword.get(config, :secret)}]

    Logger.debug("DELETE #{full_url}")

    case HTTPoison.delete(full_url, headers, ssl: [{:versions, [:"tlsv1.2"]}], hackney: hackney) do
      {:ok, %HTTPoison.Response{status_code: 200, body: body}} ->
        body

      {:ok, %HTTPoison.Response{status_code: 201, body: body}} ->
        body

      {:ok, %HTTPoison.Response{status_code: 404}} ->
        IO.puts("Not found :(")

      {:error, %HTTPoison.Error{reason: reason}} ->
        IO.inspect(reason)
    end
  end
end
