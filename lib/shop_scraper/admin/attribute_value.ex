defmodule ShopScraper.Admin.AttributeValue do
  use Ecto.Schema
  import Ecto.Changeset


  schema "attribute_values" do
    field :value, :string
    belongs_to :product, ShopScraper.Admin.Product, on_replace: :nilify
    belongs_to :attribute, ShopScraper.Admin.Attribute, on_replace: :nilify

    timestamps()
  end

  @doc false
  def changeset(attribute_value, attrs) do
    attribute_value
    |> cast(attrs, [:value, :product_id, :attribute_id])
    |> validate_required([:value])
    |> put_assoc(:attribute, attrs[:attribute])
    |> put_assoc(:product, attrs[:product])
  end

  def display(nil), do: ""
  def display(attribute_value), do: "#{attribute_value.value}"

  def select_search_field() do
    :value
  end
end
