defmodule ShopScraper.Admin.Product do
  use Ecto.Schema
  import Ecto.Changeset

  alias ShopScraper.Admin.Price

  schema "products" do
    field :name, :string
    field :sku, :string
    field :woocommerce_id, :integer
    has_many :prices, Price, on_replace: :delete
    has_many :attribute_values, ShopScraper.Admin.AttributeValue

    timestamps()
  end

  @doc false
  def changeset(product, attrs) do
    product
    |> cast(attrs, [:name, :sku, :woocommerce_id])
    |> validate_required([:name])
    |> cast_assoc(:prices)
    |> cast_assoc(:attribute_values)
    |> unique_constraint(:name)
  end

  @doc false
  def scraper_changeset(product, attrs) do
    product
    |> cast(attrs, [:name, :sku, :woocommerce_id])
    |> validate_required([:name])
    # |> cast_assoc(:prices, with: &ShopScraper.Admin.Price.scraper_changeset/2)
    # |> put_assoc(:prices, Enum.map(prices, ))
    |> unique_constraint(:name)
  end

  def display(nil), do: ""
  def display(product), do: "#{product.name}"

  def select_search_field() do
    :name
  end
end
