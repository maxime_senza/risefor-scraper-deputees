defmodule ShopScraper.Admin.AttributeParams do
  use Ecto.Schema
  import Ecto.Changeset


  schema "attribute_params" do
    field :params, :map
    belongs_to :shop, ShopScraper.Admin.Shop, on_replace: :nilify
    belongs_to :attribute, ShopScraper.Admin.Attribute, on_replace: :nilify

    timestamps()
  end

  @doc false
  def changeset(attribute_params, attrs) do
    attribute_params
    |> cast(attrs, [:params, :shop_id, :attribute_id])
    |> validate_required([:params])
    |> cast_assoc(:shop)
    |> cast_assoc(:attribute)
  end

  def display(nil), do: ""
  def display(attribute_params), do: "#{attribute_params.params}"

  def select_search_field() do
    :params
  end
end
