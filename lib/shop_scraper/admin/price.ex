defmodule ShopScraper.Admin.Price do
  use Ecto.Schema
  import Ecto.Changeset


  schema "prices" do
    field :amount, :decimal
    field :quantity, :integer
    field :url, :string
    field :woocommerce_id, :integer
    belongs_to :product, ShopScraper.Admin.Product, on_replace: :nilify
    belongs_to :shop, ShopScraper.Admin.Shop, on_replace: :nilify

    timestamps()
  end

  @doc false
  def changeset(price, attrs) do
    price
    |> cast(attrs, [:quantity, :amount, :url, :woocommerce_id, :product_id, :shop_id])
    |> validate_required([:quantity, :amount, :url])
    # |> cast_assoc(:product)
    |> put_assoc(:shop, attrs[:shop])
    # |> foreign_key_constraint(:product_id)
    |> put_assoc(:product, attrs[:product])
  end

  @doc false
  def scraper_changeset(price, attrs) do
    # require Logger;Logger.debug(inspect(attrs))
    price
    |> cast(attrs, [:quantity, :amount, :url, :woocommerce_id])
    |> validate_required([:quantity, :amount, :url])
    # |> cast_assoc(:product)
    |> put_assoc(:shop, attrs[:shop])
    # |> foreign_key_constraint(:product_id)
    # |> cast_assoc(:product)
  end

  def display(nil), do: ""
  def display(price), do: "#{price.quantity}"

  def select_search_field() do
    :quantity
  end
end
