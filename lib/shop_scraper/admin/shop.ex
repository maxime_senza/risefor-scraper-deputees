defmodule ShopScraper.Admin.Shop do
  use Ecto.Schema
  import Ecto.Changeset


  schema "shops" do
    field :name, :string
    field :scraper_params, :map
    has_many :prices, ShopScraper.Admin.Price

    timestamps()
  end

  @doc false
  def changeset(shop, attrs) do
    shop
    |> cast(attrs, [:name, :scraper_params])
    |> validate_required([:name, :scraper_params])
    |> unique_constraint(:name)
  end

  def display(nil), do: ""
  def display(shop), do: "#{shop.name}"

  def select_search_field() do
    :name
  end
end
