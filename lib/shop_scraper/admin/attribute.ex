defmodule ShopScraper.Admin.Attribute do
  use Ecto.Schema
  import Ecto.Changeset


  schema "attributes" do
    field :name, :string
    field :params, :map
    field :woocommerce_id, :integer

    timestamps()
  end

  @doc false
  def changeset(attribute, attrs) do
    attribute
    |> cast(attrs, [:name, :params, :woocommerce_id])
    |> validate_required([:name, :params])
    |> unique_constraint(:name)
  end

  def display(nil), do: ""
  def display(attribute), do: "#{attribute.name}"

  def select_search_field() do
    :name
  end
end
