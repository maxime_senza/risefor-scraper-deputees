defmodule ShopScraper.Admin do
  @moduledoc """
  The Admin context.
  """

  import Ecto.Query, warn: false
  alias ShopScraper.Repo
  alias Forage

  alias ShopScraper.Admin.Shop
  alias ShopScraper.Admin.Price
  alias ShopScraper.Admin.AttributeValue

  @doc """
  Returns a filtered list of shops.

  The list is filtered by `params["_search"]`, sorted by `params["_sort"]
  and paginated according to `params["_pagination"].

  ## Examples

      iex> list_shops(params)
      [%Shop{}, ...]

  """
  def list_shops(params) do
    Forage.paginate(params, Shop, Repo, sort: [:name, :id], preload: [])
  end

  @doc """
  Gets a single shop.

  Raises `Ecto.NoResultsError` if the Shop does not exist.

  ## Examples

      iex> get_shop!(123)
      %Shop{}

      iex> get_shop!(456)
      ** (Ecto.NoResultsError)

  """
  def get_shop!(id), do: Repo.get!(Shop, id) |> Repo.preload([prices: [:product, :shop]])

  @doc """
  Gets shop by name
  """
  def get_shop_by_name(name) do
    Repo.one(from s in Shop, where: s.name == ^name)
    |> Repo.preload([])
  end

  @doc """
  Creates a shop.

  ## Examples

      iex> create_shop(%{field: value})
      {:ok, %Shop{}}

      iex> create_shop(%{field: bad_value})
      {:error, %Ecto.Changeset{}}

  """
  def create_shop(attrs \\ %{}) do
    %Shop{}
    |> Shop.changeset(attrs)
    |> Repo.insert()
  end

  @doc """
  Updates a shop.

  ## Examples

      iex> update_shop(shop, %{field: new_value})
      {:ok, %Shop{}}

      iex> update_shop(shop, %{field: bad_value})
      {:error, %Ecto.Changeset{}}

  """
  def update_shop(%Shop{} = shop, attrs) do
    shop
    |> Shop.changeset(attrs)
    |> Repo.update()
  end

  @doc """
  Deletes a Shop.

  ## Examples

      iex> delete_shop(shop)
      {:ok, %Shop{}}

      iex> delete_shop(shop)
      {:error, %Ecto.Changeset{}}

  """
  def delete_shop(%Shop{} = shop) do
    Repo.delete(shop)
  end

  @doc """
  Returns an `%Ecto.Changeset{}` for tracking shop changes.

  ## Examples

      iex> change_shop(shop)
      %Ecto.Changeset{source: %Shop{}}

  """
  def change_shop(%Shop{} = shop) do
    Shop.changeset(shop, %{})
  end

  alias ShopScraper.Admin.Attribute

  @doc """
  Returns a filtered list of attributes.

  The list is filtered by `params["_search"]`, sorted by `params["_sort"]
  and paginated according to `params["_pagination"].

  ## Examples

      iex> list_attributes(params)
      [%Attribute{}, ...]

  """
  def list_attributes(params) do
    Forage.paginate(params, Attribute, Repo, sort: [:name, :id], preload: [])
  end

  @doc """
  Gets a single attribute.

  Raises `Ecto.NoResultsError` if the Attribute does not exist.

  ## Examples

      iex> get_attribute!(123)
      %Attribute{}

      iex> get_attribute!(456)
      ** (Ecto.NoResultsError)

  """
  def get_attribute!(id), do: Repo.get!(Attribute, id) |> Repo.preload([])

  @doc """
  Gets attribute by woocommerce_id
  """
  def get_attribute_by_woocommerce_id(wid) do
    Repo.one(from p in Attribute, where: p.woocommerce_id == ^wid)
    |> Repo.preload([])
  end

  @doc """
  Creates a attribute.

  ## Examples

      iex> create_attribute(%{field: value})
      {:ok, %Attribute{}}

      iex> create_attribute(%{field: bad_value})
      {:error, %Ecto.Changeset{}}

  """
  def create_attribute(attrs \\ %{}) do
    %Attribute{}
    |> Attribute.changeset(attrs)
    |> Repo.insert()
  end

  @doc """
  Updates a attribute.

  ## Examples

      iex> update_attribute(attribute, %{field: new_value})
      {:ok, %Attribute{}}

      iex> update_attribute(attribute, %{field: bad_value})
      {:error, %Ecto.Changeset{}}

  """
  def update_attribute(%Attribute{} = attribute, attrs) do
    attribute
    |> Attribute.changeset(attrs)
    |> Repo.update()
  end

  @doc """
  Deletes a Attribute.

  ## Examples

      iex> delete_attribute(attribute)
      {:ok, %Attribute{}}

      iex> delete_attribute(attribute)
      {:error, %Ecto.Changeset{}}

  """
  def delete_attribute(%Attribute{} = attribute) do
    Repo.delete(attribute)
  end

  @doc """
  Returns an `%Ecto.Changeset{}` for tracking attribute changes.

  ## Examples

      iex> change_attribute(attribute)
      %Ecto.Changeset{source: %Attribute{}}

  """
  def change_attribute(%Attribute{} = attribute) do
    Attribute.changeset(attribute, %{})
  end

  alias ShopScraper.Admin.Product

  @doc """
  Returns a filtered list of products.

  The list is filtered by `params["_search"]`, sorted by `params["_sort"]
  and paginated according to `params["_pagination"].

  ## Examples

      iex> list_products(params)
      [%Product{}, ...]

  """
  def list_products(params) do
    Forage.paginate(params, Product, Repo, sort: [:name, :id], preload: [])
  end

  @doc """
  Gets a single product.

  Raises `Ecto.NoResultsError` if the Product does not exist.

  ## Examples

      iex> get_product!(123)
      %Product{}

      iex> get_product!(456)
      ** (Ecto.NoResultsError)

  """
  def get_product!(id), do: Repo.get!(Product, id) |> Repo.preload([prices: [:product, :shop], attribute_values: []])

  @doc """
  Gets product by name
  """
  def get_product_by_name(name) do
    Repo.one(from p in Product, where: p.name == ^name)
    |> Repo.preload([prices: [:product, :shop], attribute_values: []])
  end

  @doc """
  Creates a product.

  ## Examples

      iex> create_product(%{field: value})
      {:ok, %Product{}}

      iex> create_product(%{field: bad_value})
      {:error, %Ecto.Changeset{}}

  """
  def create_product(attrs \\ %{}) do
    %Product{}
    |> Product.changeset(attrs)
    |> Repo.insert(on_conflict: :replace_all_except_primary_key, conflict_target: [:name])
  end

  def create_attribute_value(attrs \\ %{}) do
    %AttributeValue{}
    |> AttributeValue.changeset(attrs)
    |> Repo.insert(on_conflict: :replace_all_except_primary_key, conflict_target: [:product_id, :attribute_id])
  end

  def create_price(attrs \\ %{}) do
    %Price{}
    |> Price.changeset(attrs)
    |> Repo.insert(on_conflict: :replace_all_except_primary_key, conflict_target: [:product_id, :shop_id, :quantity])
  end

  @doc """
  Updates a product.

  ## Examples

      iex> update_product(product, %{field: new_value})
      {:ok, %Product{}}

      iex> update_product(product, %{field: bad_value})
      {:error, %Ecto.Changeset{}}

  """
  def update_product(%Product{} = product, attrs) do
    product
    |> Product.changeset(attrs)
    |> Repo.update()
  end

  @doc """
  Deletes a Product.

  ## Examples

      iex> delete_product(product)
      {:ok, %Product{}}

      iex> delete_product(product)
      {:error, %Ecto.Changeset{}}

  """
  def delete_product(%Product{} = product) do
    Repo.delete(product)
  end

  @doc """
  Returns an `%Ecto.Changeset{}` for tracking product changes.

  ## Examples

      iex> change_product(product)
      %Ecto.Changeset{source: %Product{}}

  """
  def change_product(%Product{} = product) do
    Product.changeset(product, %{})
  end
end
