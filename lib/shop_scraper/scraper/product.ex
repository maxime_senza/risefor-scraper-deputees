defmodule ShopScraper.Scraper.Product do
  alias ShopScraper.Scraper.ProductSupervisor
  alias ShopScraper.Scraper.Product
  alias ShopScraper.Admin.Price

  require Meeseeks.XPath

  use GenServer

  defmodule State do
    defstruct [
      :shop,
      :product_url,
      :changeset,
      :shop_server
    ]
  end

  def start_link(params) do
    GenServer.start_link(__MODULE__, params)
  end

  def scrape(pid) do
    GenServer.cast(pid, :scrape_product)
  end

  def init(args), do: init(args, %State{})

  def init([{:shop_server, shop_server} | rest], state) do
    init(rest, %{state | shop_server: shop_server})
  end

  def init([{:shop, shop} | rest], state) do
    init(rest, %{state | shop: shop})
  end

  def init([{:product_url, product_url} | rest], state) do
    init(rest, %{state | product_url: product_url})
  end

  def init([], state) do
    require Logger; Logger.debug "Started product handler for url #{state.product_url}"
    {:ok, state}
  end

  def handle_cast(:scrape_product, %{shop: shop, product_url: product_url} = state) do
    require Logger; Logger.debug "Started product scrape for url #{state.product_url}"
    %{id: shop_id,
      scraper_params:
      %{"product" =>
        %{"name" => name,
          "prices" => prices,
          "attributes" => _
        }}} = shop
    html = get_html(product_url)
    product_name = scrape_product_name(html, name)

    product = case ShopScraper.Admin.get_product_by_name(product_name) do
      nil -> %ShopScraper.Admin.Product{}
      product -> product
    end
    found_prices =
      Enum.map(prices, &scrape_product_price(html, &1))
      |> Enum.filter(fn {_quantity, amount} -> amount != nil end)
    new_prices =
      if product.id == nil do
        Enum.map(found_prices, fn
          {quantity, amount} ->
            Price.scraper_changeset(%Price{}, %{shop: shop, product: product, quantity: quantity, amount: amount, url: product_url})
        end)
      else
        Enum.reduce(found_prices, [], fn
          {quantity, amount}, acc ->
            case Enum.find(product.prices, &match?(%Price{shop_id: ^shop_id, quantity: ^quantity}, &1)) do
              nil -> [ Price.scraper_changeset(%Price{}, %{shop: shop, product: product, quantity: quantity, amount: amount, url: product_url}) | acc]
              _price -> acc
            end
        end)
      end
    updated_prices =
      if product.id == nil do
        []
      else
        product.prices
        # |> Enum.filter(fn %Price{shop: price_shop} -> price_shop == shop end)
        |> Enum.map(fn
          %Price{shop_id: ^shop_id, quantity: quantity} = price ->
            case List.keyfind(found_prices, quantity, 0) do
              {_quantity, amount} -> Price.scraper_changeset(price, %{amount: amount, url: product_url, shop: shop})
              nil -> nil
            end
          price -> price
        end)
        |> Enum.reject(&is_nil/1)
      end
    all_prices = updated_prices ++ new_prices
      # Enum.map(product_prices, &(%{&1 | url: product_url, shop: shop}))
    require Logger;Logger.error(inspect(updated_prices))
    require Logger;Logger.error(inspect(new_prices))
    product_changeset =
      ShopScraper.Admin.Product.scraper_changeset(
        product,
        %{name: product_name})
      |> Ecto.Changeset.put_assoc(:prices, all_prices)
    require Logger;Logger.debug(inspect(product_changeset))
    send(state.shop_server, {:new_product, product_changeset})

    {:noreply, %{state | changeset: product_changeset}}
  end

  defp scrape_product_name(html, [%{"xpath" => xpath}]) do
    selector = Meeseeks.XPath.xpath(xpath)
    Meeseeks.one(html, selector)
    |> Meeseeks.text()
    # name = fetch_text(product_url, scraper_params.name)
    # attributes = Enum.map(scraper_params.attributes, fn (name, params) -> {name, fetch_text(product_url, params)} end)
  end

  defp scrape_product_price(html, %{"quantity" => quantity, "xpath" => xpath}) when is_integer(quantity) do
    selector = Meeseeks.XPath.xpath(xpath)
    amount = Meeseeks.one(html, selector)
             |> Meeseeks.text()
    amount_parsed = case amount && Regex.run(~r/(\d+\.\d+)/, amount, capture: :all_but_first) do
      nil -> nil
      [match] -> match
    end
    amount_float = case amount_parsed && Float.parse(amount_parsed) do
      nil -> nil
      :error -> nil
      {amount, _} -> amount
    end
    # %{quantity: quantity, amount: amount_float, url: nil, shop: nil}
    {quantity, amount_float}
    # name = fetch_text(product_url, scraper_params.name)
    # attributes = Enum.map(scraper_params.attributes, fn (name, params) -> {name, fetch_text(product_url, params)} end)
  end

  def get_html(url) do
    HTTPoison.get!(url, [], timeout: 50_000, recv_timeout: 50_000, follow_redirect: true, ssl: [{:versions, [:"tlsv1.2"]}]).body
  end
end
