defmodule ShopScraper.Scraper.Supervisor do
  use DynamicSupervisor

  def start_link(_) do
    DynamicSupervisor.start_link(__MODULE__, [], name: __MODULE__)
  end

  @impl true
  def init(_) do
    DynamicSupervisor.init(strategy: :one_for_one)
  end

  def start_scraper(shop, params \\ []) do
    case Registry.lookup(ShopScraper.Scraper.Registry, shop) do
      [{_, _}] -> nil
      [] ->
        spec = {ShopScraper.Scraper.ShopSupervisor, [shop, params]}
        DynamicSupervisor.start_child(__MODULE__, spec)
    end
  end

  def stop_scraper(shop) when is_atom(shop) do
    [{_, shop_sup}] = Registry.lookup(ShopScraper.Scraper.Registry, shop)
    DynamicSupervisor.terminate_child(__MODULE__, shop_sup)
  end
end
