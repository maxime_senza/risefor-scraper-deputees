defmodule ShopScraper.Scraper.ShopSupervisor do
  use Supervisor

  def start_link(args) when is_list(args) do
    Supervisor.start_link(__MODULE__, args)
  end

  def init([shop, params]) do
    require Logger; Logger.debug "Started ShopSupervisor"
    {:ok, _} = Registry.register(ShopScraper.Scraper.Registry, shop, self())

    children = [
      {ShopScraper.Scraper.Shop, [{:shop, shop}, {:shop_sup, self()} | params]}
    ]

    Supervisor.init(children, strategy: :one_for_all)
  end
end
