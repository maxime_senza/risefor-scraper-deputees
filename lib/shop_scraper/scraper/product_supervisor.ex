defmodule ShopScraper.Scraper.ProductSupervisor do
  use DynamicSupervisor

  def start_link(args) when is_list(args) do
    DynamicSupervisor.start_link(__MODULE__, args)
  end

  def init(_arg) do
    require Logger; Logger.debug "Started ProductSupervisor"
    DynamicSupervisor.init(strategy: :one_for_one)
  end

  defdelegate start_product(sup, spec), to: DynamicSupervisor, as: :start_child
end
