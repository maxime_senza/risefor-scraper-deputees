defmodule ShopScraper.Scraper do
  alias ShopScraper.Scraper.Supervisor
  alias ShopScraper.Scraper.Shop

  defdelegate start_scraper(shop, params), to: Supervisor
  defdelegate stop_scraper(shop), to: Supervisor

  defdelegate fetch_products(shop), to: Shop

  defdelegate get_results(shop), to: Shop
  defdelegate apply(shop), to: Shop
end
