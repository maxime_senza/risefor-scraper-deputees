defmodule ShopScraper.Scraper.Shop do
  alias ShopScraper.Scraper.ProductSupervisor
  alias ShopScraper.Scraper.Product

  require Meeseeks.XPath
  require Logger
  require IEx

  use GenServer

  defmodule State do
    defstruct [
      :shop,
      :shop_sup, :product_sup,
      products: [],
      product_servers: []
    ]
  end

  def start_link([shop, params]) do
    GenServer.start_link(__MODULE__, [shop, params])
  end

  def fetch_products(shop) do
    [{_, shop_pid}] = Registry.lookup(ShopScraper.Scraper.ShopRegistry, shop)
    GenServer.cast(shop_pid, :scrape_shop)
  end

  def get_results(shop) do
    [{_, shop_pid}] = Registry.lookup(ShopScraper.Scraper.ShopRegistry, shop)
    GenServer.call(shop_pid, :get_products, :infinity)
  end

  def apply(shop) do
    [{_, shop_pid}] = Registry.lookup(ShopScraper.Scraper.ShopRegistry, shop)
    GenServer.call(shop_pid, :apply)
  end

  def init(args), do: init(args, %State{})

  def init([{:shop, shop} | rest], state) do
    init(rest, %{state | shop: shop})
  end

  def init([{:shop_sup, shop_sup} | rest], state) do
    init(rest, %{state | shop_sup: shop_sup})
  end

  def init([], state) do
    require Logger; Logger.debug "Started shop handler for shop #{state.shop.name}"
    {:ok, _} = Registry.register(ShopScraper.Scraper.ShopRegistry, state.shop, self())
    Process.flag(:trap_exit, true)
    send(self(), :start_product_sup)

    {:ok, state}
  end

  def scrape_shop() do
  end

  def handle_info(:start_product_sup, %State{shop_sup: shop_sup} = state) do
    {:ok, product_sup} = Supervisor.start_child(shop_sup, ProductSupervisor)
    Process.link(product_sup)

    {:noreply, %{state | product_sup: product_sup}}
  end

  def handle_info({:new_product, product}, %State{products: products} = state) do
    {:noreply, %{state | products: [product | products]}}
  end

  defp get_first(urls) when is_list(urls), do: urls[0]
  defp get_first(url), do: url

  def handle_cast(:scrape_shop, %State{product_servers: [], shop: shop} = state) do
    %{scraper_params:
      %{"list" =>
        %{"url" => url,
          "next_page" => next_page,
          "product" => product } } } = shop
    product_urls =
      get_all_list_pages(url, next_page)
      |> get_all_product_urls(product)
      |> Enum.map(fn product_url ->
           url = get_first(url)
           URI.merge(url, product_url) |> to_string()
         end)
      # |> Enum.take(5)
    Logger.info(inspect(product_urls))
    product_servers = for product_url <- product_urls do
      server_pid = new_product_server(product_url, state)
      Product.scrape(server_pid)
      {product_url, server_pid}
    end
    {:noreply, %{state | product_servers: product_servers}}
  end
  def handle_cast(:scrape_shop, state), do: {:noreply, state}

  defp new_product_server(product_url, %State{product_sup: product_sup, shop: shop}) do
    child_params = [
      shop: shop,
      product_url: product_url,
      shop_server: self()
    ]
    child_spec = Supervisor.child_spec({Product, child_params}, restart: :temporary)
    {:ok, pid} = ProductSupervisor.start_product(product_sup, child_spec)
    true = Process.link(pid)
    pid
  end

  def handle_call(:get_products, _from, %State{products: products} = state) do
    {:reply, products, state}
  end

  def handle_call(:apply, _from, %State{products: products} = state) do
    changesets =
      Enum.map(products, fn (product) ->
        product
        |> ShopScraper.Repo.insert(on_conflict: :nothing) # on_conflict: :replace_all_except_primary_key, conflict_target: [:name])
      end)
    {:reply, changesets, state}
  end

  defp get_all_list_pages(urls, next_page) when is_list(urls) do
    Enum.flat_map(urls, &get_all_list_pages(&1, next_page, []))
  end
  defp get_all_list_pages(url, next_page) do
    get_all_list_pages(url, next_page, [])
  end
  defp get_all_list_pages(nil, _, htmls), do: htmls
  defp get_all_list_pages(url, [%{"xpath" => xpath}] = next_page, htmls) do
    Logger.debug("Scraping list page at #{url}")
    html = get_html(url)
    selector = Meeseeks.XPath.xpath(xpath)
    next_page_url =
      Meeseeks.one(html, selector)
      |> Meeseeks.attr("href")
    Logger.debug("Found next page link at #{next_page_url}")
    # selector = %SweetXpath{path: to_char_list(xpath), is_optional: true, cast_to: :string}
    # next_page_url = SweetXml.xpath(html, selector)
    case next_page_url do
      ^url -> [html | htmls]
      _ -> get_all_list_pages(next_page_url, next_page, [html | htmls])
    end
  end

  defp get_all_product_urls(htmls, [%{"xpath" => xpath}]) do
    htmls
    |> Enum.map(fn html ->
      selector = Meeseeks.XPath.xpath(xpath)
      Meeseeks.all(html, selector)
      |> Enum.map(&Meeseeks.attr(&1, "href"))
    end)
    |> List.flatten()
    |> Enum.uniq()
  end

  def get_html(url) do
    HTTPoison.get!(url, [], timeout: 50_000, recv_timeout: 50_000, follow_redirect: true, ssl: [{:versions, [:"tlsv1.2"]}]).body
  end
end
