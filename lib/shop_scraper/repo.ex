defmodule ShopScraper.Repo do
  use Ecto.Repo,
    otp_app: :shop_scraper,
    adapter: Ecto.Adapters.Postgres

  use Paginator
end
