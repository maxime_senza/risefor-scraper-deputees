defmodule ShopScraperWeb.Router do
  require Mandarin.Router

  use ShopScraperWeb, :router

  pipeline :browser do
    plug :accepts, ["html"]
    plug :fetch_session
    plug :fetch_flash
    plug :protect_from_forgery
    plug :put_secure_browser_headers
  end

  pipeline :api do
    plug :accepts, ["json"]
  end

  pipeline :admin_layout do
    plug(:put_layout, {ShopScraperWeb.AdminLayoutView, "layout.html"})
  end

  scope "/", ShopScraperWeb do
    pipe_through :browser

    get "/", PageController, :index
  end

  # Other scopes may use custom stacks.
  # scope "/api", ShopScraperWeb do
  #   pipe_through :api
  # end

  scope "/admin", ShopScraperWeb.Admin, as: :admin do
    pipe_through([:browser, :admin_layout])

    Mandarin.Router.resources "/shops", ShopController
    post "/shops/:id/test", ShopController, :test
    get "/shops/:id/results", ShopController, :results
    post "/shops/:id/apply", ShopController, :apply

    Mandarin.Router.resources "/attributes", AttributeController
    post "/attributes/fetch_from_woocommerce", AttributeController, :fetch_from_woocommerce
    Mandarin.Router.resources "/products", ProductController
    Mandarin.Router.resources "/jobs", JobController
    Mandarin.Router.resources "/prices", PriceController
  end
end
