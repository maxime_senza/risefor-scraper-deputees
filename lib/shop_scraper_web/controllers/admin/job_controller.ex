defmodule ShopScraperWeb.Admin.JobController do
  use ShopScraperWeb, :controller

  alias ShopScraper.Admin
  alias ShopScraper.Admin.Shop
  alias ShopScraper.Scraper
  alias ForageWeb.ForageController

  # Adds the the resource type to the conn
  plug Mandarin.Plugs.Resource, :shop

end
