defmodule ShopScraperWeb.Admin.ShopController do
  use ShopScraperWeb, :controller

  alias ShopScraper.Admin
  alias ShopScraper.Admin.Shop
  alias ShopScraper.Scraper
  alias ForageWeb.ForageController

  # Adds the the resource type to the conn
  plug Mandarin.Plugs.Resource, :shop

  def index(conn, params) do
    shops = Admin.list_shops(params)
    render(conn, "index.html", shops: shops)
  end

  def new(conn, _params) do
    changeset = Admin.change_shop(%Shop{})
    render(conn, "new.html", changeset: changeset)
  end

  def create(conn, %{"shop" => shop_params}) do
    shop_params = shop_params |> update_in(["scraper_params"], &Jason.decode!/1)
    case Admin.create_shop(shop_params) do
      {:ok, shop} ->
        conn
        |> put_flash(:info, "Shop created successfully.")
        |> redirect(to: Routes.admin_shop_path(conn, :show, shop))

      {:error, %Ecto.Changeset{} = changeset} ->
        render(conn, "new.html", changeset: changeset)
    end
  end

  def show(conn, %{"id" => id}) do
    shop = Admin.get_shop!(id)
    render(conn, "show.html", shop: shop, prices: shop.prices)
  end

  def edit(conn, %{"id" => id}) do
    shop = Admin.get_shop!(id)
    changeset = Admin.change_shop(shop)
    render(conn, "edit.html", shop: shop, changeset: changeset)
  end

  def update(conn, %{"id" => id, "shop" => shop_params}) do
    shop_params = shop_params |> update_in(["scraper_params"], &Jason.decode!/1)
    shop = Admin.get_shop!(id)

    case Admin.update_shop(shop, shop_params) do
      {:ok, shop} ->
        conn
        |> put_flash(:info, "Shop updated successfully.")
        |> redirect(to: Routes.admin_shop_path(conn, :show, shop))

      {:error, %Ecto.Changeset{} = changeset} ->
        render(conn, "edit.html", shop: shop, changeset: changeset)
    end
  end

  def delete(conn, %{"id" => id} = params) do
    shop = Admin.get_shop!(id)
    {:ok, _shop} = Admin.delete_shop(shop)
    # After deleting, remain on the same page
    redirect_params = Map.take(params, ["_pagination"])

    conn
    |> put_flash(:info, "Shop deleted successfully.")
    |> redirect(to: Routes.admin_shop_path(conn, :index, redirect_params))
  end

  def select(conn, params) do
    shops = Admin.list_shops(params)
    data = ForageController.forage_select_data(shops, &Shop.display/1)
    json(conn, data)
  end

  def test(conn, %{"id" => id} = params) do
    shop = Admin.get_shop!(id)

    ShopScraper.Scraper.start_scraper(shop, [])
    ShopScraper.Scraper.fetch_products(shop)

    conn
    |> put_flash(:info, "Scraper started.")
    |> redirect(to: Routes.admin_shop_path(conn, :results, shop))
  end

  def results(conn, %{"id" => id} = _params) do
    shop = Admin.get_shop!(id)

    results = ShopScraper.Scraper.get_results(shop)

    render(conn, "test.html", shop: shop, results: results)
  end

  def apply(conn, %{"id" => id} = _params) do
    shop = Admin.get_shop!(id)

    results = ShopScraper.Scraper.apply(shop)

    # render(conn, "test.html", shop: shop, results: results)
    conn
    |> put_flash(:info, "Updated #{Enum.count(results)} products")
    |> redirect(to: Routes.admin_shop_path(conn, :index))
  end
end
