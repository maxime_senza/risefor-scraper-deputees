defmodule ShopScraperWeb.Admin.ProductController do
  use ShopScraperWeb, :controller

  alias ShopScraper.Admin
  alias ShopScraper.Admin.Product
  alias ForageWeb.ForageController

  # Adds the the resource type to the conn
  plug Mandarin.Plugs.Resource, :product

  def index(conn, params) do
    products = Admin.list_products(params)
    render(conn, "index.html", products: products)
  end

  def new(conn, _params) do
    changeset = Admin.change_product(%Product{})
    render(conn, "new.html", changeset: changeset)
  end

  def create(conn, %{"product" => product_params}) do
    case Admin.create_product(product_params) do
      {:ok, product} ->
        conn
        |> put_flash(:info, "Product created successfully.")
        |> redirect(to: Routes.admin_product_path(conn, :show, product))

      {:error, %Ecto.Changeset{} = changeset} ->
        render(conn, "new.html", changeset: changeset)
    end
  end

  def show(conn, %{"id" => id}) do
    product = Admin.get_product!(id)
    render(conn, "show.html", product: product, prices: product.prices)
  end

  def edit(conn, %{"id" => id}) do
    product = Admin.get_product!(id)
    changeset = Admin.change_product(product)
    render(conn, "edit.html", product: product, changeset: changeset)
  end

  def update(conn, %{"id" => id, "product" => product_params}) do
    product = Admin.get_product!(id)

    case Admin.update_product(product, product_params) do
      {:ok, product} ->
        conn
        |> put_flash(:info, "Product updated successfully.")
        |> redirect(to: Routes.admin_product_path(conn, :show, product))

      {:error, %Ecto.Changeset{} = changeset} ->
        render(conn, "edit.html", product: product, changeset: changeset)
    end
  end

  def delete(conn, %{"id" => id} = params) do
    product = Admin.get_product!(id)
    {:ok, _product} = Admin.delete_product(product)
    # After deleting, remain on the same page
    redirect_params = Map.take(params, ["_pagination"])

    conn
    |> put_flash(:info, "Product deleted successfully.")
    |> redirect(to: Routes.admin_product_path(conn, :index, redirect_params))
  end

  def select(conn, params) do
    products = Admin.list_products(params)
    data = ForageController.forage_select_data(products, &Product.display/1)
    json(conn, data)
  end
end
