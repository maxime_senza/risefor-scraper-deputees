defmodule ShopScraperWeb.Admin.AttributeController do
  use ShopScraperWeb, :controller

  alias ShopScraper.Admin
  alias ShopScraper.Admin.Attribute
  alias ForageWeb.ForageController

  # Adds the the resource type to the conn
  plug Mandarin.Plugs.Resource, :attribute

  def index(conn, params) do
    attributes = Admin.list_attributes(params)
    render(conn, "index.html", attributes: attributes)
  end

  def new(conn, _params) do
    changeset = Admin.change_attribute(%Attribute{})
    render(conn, "new.html", changeset: changeset)
  end

  def create(conn, %{"attribute" => attribute_params}) do
    case Admin.create_attribute(attribute_params) do
      {:ok, attribute} ->
        conn
        |> put_flash(:info, "Attribute created successfully.")
        |> redirect(to: Routes.admin_attribute_path(conn, :show, attribute))

      {:error, %Ecto.Changeset{} = changeset} ->
        render(conn, "new.html", changeset: changeset)
    end
  end

  def show(conn, %{"id" => id}) do
    attribute = Admin.get_attribute!(id)
    render(conn, "show.html", attribute: attribute)
  end

  def edit(conn, %{"id" => id}) do
    attribute = Admin.get_attribute!(id)
    changeset = Admin.change_attribute(attribute)
    render(conn, "edit.html", attribute: attribute, changeset: changeset)
  end

  def update(conn, %{"id" => id, "attribute" => attribute_params}) do
    attribute = Admin.get_attribute!(id)

    case Admin.update_attribute(attribute, attribute_params) do
      {:ok, attribute} ->
        conn
        |> put_flash(:info, "Attribute updated successfully.")
        |> redirect(to: Routes.admin_attribute_path(conn, :show, attribute))

      {:error, %Ecto.Changeset{} = changeset} ->
        render(conn, "edit.html", attribute: attribute, changeset: changeset)
    end
  end

  def delete(conn, %{"id" => id} = params) do
    attribute = Admin.get_attribute!(id)
    {:ok, _attribute} = Admin.delete_attribute(attribute)
    # After deleting, remain on the same page
    redirect_params = Map.take(params, ["_pagination"])

    conn
    |> put_flash(:info, "Attribute deleted successfully.")
    |> redirect(to: Routes.admin_attribute_path(conn, :index, redirect_params))
  end

  def select(conn, params) do
    attributes = Admin.list_attributes(params)
    data = ForageController.forage_select_data(attributes, &Attribute.display/1)
    json(conn, data)
  end

  def fetch_from_woocommerce(conn, _params) do
    results =
      ShopScraper.Woocommerce.Attribute.fetch_all_attributes()
      |> Enum.reduce(%{attributes_count: 0, shops_count: 0}, fn
        ({:ok, %ShopScraper.Admin.Attribute{}}, acc) ->
          Map.update!(acc, :attributes_count, &(&1 + 1))
        ({:ok, %ShopScraper.Admin.Shop{}}, acc) ->
          Map.update!(acc, :shops_count, &(&1 + 1))
        (_, acc) ->
          acc
      end)

    require Logger; Logger.debug("Created #{results.attributes_count} attributes and #{results.shops_count} shops")
    conn
    |> put_flash(:info, "Created #{results.attributes_count} attributes and #{results.shops_count} shops")
    |> redirect(to: Routes.admin_attribute_path(conn, :index, %{}))
  end
end
