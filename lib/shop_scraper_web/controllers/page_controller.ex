defmodule ShopScraperWeb.PageController do
  use ShopScraperWeb, :controller

  def index(conn, _params) do
    render(conn, "index.html")
  end
end
