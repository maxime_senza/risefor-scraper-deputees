defmodule ShopScraperWeb.AdminLayoutView do
  use ShopScraperWeb, :view
  # Add Forage routes!
end

defimpl Phoenix.HTML.Safe, for: Map do
  def to_iodata(data) do
    data |> Jason.encode!() |> Jason.Formatter.pretty_print_to_iodata()
  end
end
