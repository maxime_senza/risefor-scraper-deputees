defmodule ShopScraperWeb.Admin.AttributeView do
  use ShopScraperWeb, :view
  use ForageWeb.ForageView,
    routes_module: ShopScraperWeb.Router.Helpers,
    prefix: :admin_attribute
end
