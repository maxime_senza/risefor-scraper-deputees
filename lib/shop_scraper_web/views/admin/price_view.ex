defmodule ShopScraperWeb.Admin.PriceView do
  use ShopScraperWeb, :view
  use ForageWeb.ForageView,
    routes_module: ShopScraperWeb.Router.Helpers,
    prefix: :admin_price
end
