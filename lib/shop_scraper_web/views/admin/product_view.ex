defmodule ShopScraperWeb.Admin.ProductView do
  use ShopScraperWeb, :view
  use ForageWeb.ForageView,
    routes_module: ShopScraperWeb.Router.Helpers,
    prefix: :admin_product
end
