defmodule ShopScraperWeb.Admin.ShopView do
  use ShopScraperWeb, :view
  use ForageWeb.ForageView,
    routes_module: ShopScraperWeb.Router.Helpers,
    prefix: :admin_shop

  alias ShopScraper.Admin.Shop
  alias ShopScraper.Admin.Product
  alias ShopScraper.Admin.Price
  alias ShopScraper.Admin.AttributeValue

  def construct_result(%Ecto.Changeset{data: %Product{}, errors: errors}, _shop) when errors != [] do
    "Errors on product: #{inspect(errors)}"
  end
  def construct_result(%Ecto.Changeset{data: %Price{}, errors: errors}, _shop) when errors != [] do
    "Errors on price: #{inspect(errors)}"
  end
  def construct_result(%Ecto.Changeset{data: %Product{id: nil}, action: nil, changes: %{name: name, prices: price_changesets}}, shop) do
    """
    Create product #{name}:
    #{
      Enum.map(price_changesets, &("<br>#{construct_result(&1, shop)}"))
    }
    """
  end
  def construct_result(%Ecto.Changeset{data: %Product{name: name}, action: nil, changes: %{prices: price_changesets}}, %Shop{id: current_shop_id} = shop) when price_changesets != [] do
    """
    Product #{name}:
    #{
      Enum.map(price_changesets, &("<br>#{construct_result(&1, shop)}"))
    }
    """
  end
  def construct_result(%Ecto.Changeset{data: %Product{name: name}, action: nil, changes: %{}}, _shop) do
    """
    Unchanged product #{name}
    """
  end
  def construct_result(%Ecto.Changeset{action: :insert, data: %Price{}, changes: %{amount: amount, quantity: quantity}}, _shop) do
    "Create price: #{amount} for #{quantity}"
  end
  def construct_result(%Ecto.Changeset{action: :replace, data: %Price{amount: amount, quantity: quantity}, changes: %{}}, _shop) do
    "Delete price: #{amount} for #{quantity}"
  end
  def construct_result(%Ecto.Changeset{action: :update, data: %Price{quantity: quantity, amount: amount}, changes: %{amount: new_amount}}, _shop) do
    "Update price: #{amount} => #{new_amount} for #{quantity}"
  end
  def construct_result(%Ecto.Changeset{data: %Price{amount: amount, quantity: quantity, shop_id: shop_id}, changes: %{}}, %Shop{id: current_shop_id}) when shop_id != current_shop_id do
    "Existing price from another shop: #{amount} for #{quantity}"
  end
  def construct_result(%Ecto.Changeset{data: %Price{amount: amount, quantity: quantity}, changes: %{}}, _shop) do
    "Unchanged price: #{amount} for #{quantity}"
  end
    #"#{inspect(c)}<br>produit<br>#{Enum.map(changes, &iter_changes/1) |> Enum.join(", ")}"
  #Ecto.Changeset<action: nil,
  #changes: %{prices: [
  ##Ecto.Changeset<action: :replace, changes: %{}, errors: [], data: #ShopScraper.Admin.Price<>, valid?: true>,
  ##Ecto.Changeset<action: :replace, changes: %{}, errors: [], data: #ShopScraper.Admin.Price<>, valid?: true>,
  ##Ecto.Changeset<action: :replace, changes: %{}, errors: [], data: #ShopScraper.Admin.Price<>, valid?: true>,
  ##Ecto.Changeset<action: :insert, changes: %{amount: #Decimal<60.0>, quantity: 5, shop: #Ecto.Changeset<action: :update, changes: %{}, errors: [], data: #ShopScraper.Admin.Shop<>, valid?: true>, url: "https://amsterdammarijuanaseeds.com/california-dream-feminized-seeds"}, errors: [], data: #ShopScraper.Admin.Price<>, valid?: true>,
  ##Ecto.Changeset<action: :insert, changes: %{amount: #Decimal<108.0>, quantity: 10, shop: #Ecto.Changeset<action: :update, changes: %{}, errors: [], data: #ShopScraper.Admin.Shop<>, valid?: true>, url: "https://amsterdammarijuanaseeds.com/california-dream-feminized-seeds"}, errors: [], data: #ShopScraper.Admin.Price<>, valid?: true>,
  ##Ecto.Changeset<action: :insert, changes: %{amount: #Decimal<192.0>, quantity: 20, shop: #Ecto.Changeset<action: :update, changes: %{}, errors: [], data: #ShopScraper.Admin.Shop<>, valid?: true>, url: "https://amsterdammarijuanaseeds.com/california-dream-feminized-seeds"}, errors: [], data: #ShopScraper.Admin.Price<>, valid?: true>]},
  #errors: [],
  #data: #ShopScraper.Admin.Product<>,
  #valid?: true>

  def iter_changes({:prices, prices}) do
    prices
    |> Enum.map(&display_price/1)
    |> Enum.join("<br>")
  end

  def display_price(price) do
    inspect(price)
  end
end
