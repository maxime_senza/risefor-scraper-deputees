# Script for populating the database. You can run it as:
#
#     mix run priv/repo/seeds.exs
#
# Inside the script, you can read and write to any of your
# repositories directly:
#
#     ShopScraper.Repo.insert!(%ShopScraper.SomeSchema{})
#
# We recommend using the bang functions (`insert!`, `update!`
# and so on) as they will fail if something goes wrong.
ams_params =
%{
  list: %{
    url: "https://amsterdammarijuanaseeds.com/all-marijuana-seeds",
    next_page: [%{xpath: "//li/a[@title=\"Next\"]"}],
    product: [%{xpath: "//div[@class=\"cdz-product-top\"]/a[@class=\"product-image \"]"}]
  },
  product: %{
    name: [%{xpath: "//h1[@itemprop=\"name\"]"}],
    prices: [
      %{quantity: 5, xpath: "//li/a[@name=\"5\"]/span"},
      %{quantity: 10, xpath: "//li/a[@name=\"10\"]/span"},
      %{quantity: 20, xpath: "//li/a[@name=\"20\"]/span"}
    ],
    attributes: []
  }
}

ShopScraper.Repo.insert!(%ShopScraper.Admin.Shop{name: "Amsterdam Marijuana Seeds", scraper_params: ams_params})
