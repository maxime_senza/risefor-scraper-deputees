defmodule ShopScraper.Repo.Migrations.CreatePrices do
  use Ecto.Migration

  def change do
    create table(:prices) do
      add :quantity, :integer
      add :amount, :decimal
      add :url, :string
      add :woocommerce_id, :integer
      add :product_id, references(:products, on_delete: :nothing)
      add :shop_id, references(:shops, on_delete: :nothing)

      timestamps()
    end

    create index(:prices, [:product_id])
    create index(:prices, [:shop_id])
  end
end
