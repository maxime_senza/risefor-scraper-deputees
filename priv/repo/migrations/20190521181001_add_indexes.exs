defmodule ShopScraper.Repo.Migrations.AddIndexes do
  use Ecto.Migration

  # tables:
  # shops
  # products
  # prices
  # attributes
  # attribute_values
  # attribute_params
  #
  def change do
    create unique_index(:shops, [:name])

    create unique_index(:attributes, [:name])
    create unique_index(:attributes, [:woocommerce_id])

    create unique_index(:products, [:name])
    create unique_index(:products, [:sku])
    create unique_index(:products, [:woocommerce_id])

    drop index(:attribute_params, [:shop_id])
    drop index(:attribute_params, [:attribute_id])
    create unique_index(:attribute_params, [:shop_id, :attribute_id])

    drop index(:attribute_values, [:product_id])
    drop index(:attribute_values, [:attribute_id])
    create unique_index(:attribute_values, [:product_id, :attribute_id])

    drop index(:prices, [:product_id])
    drop index(:prices, [:shop_id])
    create unique_index(:prices, [:woocommerce_id])
    create unique_index(:prices, [:product_id, :shop_id, :quantity])
  end
end
