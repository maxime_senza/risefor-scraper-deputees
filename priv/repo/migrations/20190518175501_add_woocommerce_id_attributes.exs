defmodule ShopScraper.Repo.Migrations.AddWoocommerceIdAttributes do
  use Ecto.Migration

  def change do
    alter table("attributes") do
      add :woocommerce_id, :integer
    end
  end
end
