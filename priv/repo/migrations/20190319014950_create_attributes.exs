defmodule ShopScraper.Repo.Migrations.CreateAttributes do
  use Ecto.Migration

  def change do
    create table(:attributes) do
      add :name, :string
      add :params, :map

      timestamps()
    end

  end
end
