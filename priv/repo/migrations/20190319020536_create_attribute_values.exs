defmodule ShopScraper.Repo.Migrations.CreateAttributeValues do
  use Ecto.Migration

  def change do
    create table(:attribute_values) do
      add :value, :string
      add :product_id, references(:products, on_delete: :nothing)
      add :attribute_id, references(:attributes, on_delete: :nothing)

      timestamps()
    end

    create index(:attribute_values, [:product_id])
    create index(:attribute_values, [:attribute_id])
  end
end
