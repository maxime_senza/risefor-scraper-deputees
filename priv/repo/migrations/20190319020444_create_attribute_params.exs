defmodule ShopScraper.Repo.Migrations.CreateAttributeParams do
  use Ecto.Migration

  def change do
    create table(:attribute_params) do
      add :params, :map
      add :shop_id, references(:shops, on_delete: :nothing)
      add :attribute_id, references(:attributes, on_delete: :nothing)

      timestamps()
    end

    create index(:attribute_params, [:shop_id])
    create index(:attribute_params, [:attribute_id])
  end
end
