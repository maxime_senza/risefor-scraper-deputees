defmodule ShopScraper.Repo.Migrations.ModifyPricesIndexes do
  use Ecto.Migration

  def change do
    alter table(:prices) do
      modify :product_id, references(:products, on_delete: :delete_all), from: references(:products, on_delete: :nothing)
      modify :shop_id, references(:shops, on_delete: :delete_all), from: references(:products, on_delete: :nothing)
    end
  end
end
