defmodule ShopScraper.Repo.Migrations.ModifyPricesInAttributeValues do
  use Ecto.Migration

  def change do
    alter table(:attribute_values) do
      modify :product_id, references(:products, on_delete: :delete_all), from: references(:products, on_delete: :nothing)
      modify :attribute_id, references(:attributes, on_delete: :delete_all), from: references(:products, on_delete: :nothing)
    end
  end
end
