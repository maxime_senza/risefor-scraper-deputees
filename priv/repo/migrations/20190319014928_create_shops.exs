defmodule ShopScraper.Repo.Migrations.CreateShops do
  use Ecto.Migration

  def change do
    create table(:shops) do
      add :name, :string
      add :scraper_params, :map

      timestamps()
    end

  end
end
