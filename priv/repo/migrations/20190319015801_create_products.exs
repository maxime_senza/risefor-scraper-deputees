defmodule ShopScraper.Repo.Migrations.CreateProducts do
  use Ecto.Migration

  def change do
    create table(:products) do
      add :name, :string
      add :sku, :string
      add :woocommerce_id, :integer

      timestamps()
    end

  end
end
