# Risefor - Scraper députées & Sénateurices

Scraper permettant de mettre à jour la liste de députée ainsi que leurs informations de contact

Site utilisé pour les sources: https://www.nosdeputes.fr/

Pour mettre à jour les sénateurices: Changer le site source à https://www.nossenateurs.fr/

# Lancement du scraper
Ce placer dans le dossier /merchandscrape/ & lancer la commande `scrapy crawl get_representatives`

# Nettoyage des données récupérés
Une fois la récupération de données réalisé, vous pouvez lancer le programme de nettoyage de l'information, qui vous permettra entre autre de :
- Associer l'élu.e avec les codes postaux de sa circonscription
- Avoir un format de fichier prêt à l'utilisation sur avec l'outil risefor.org
- Nettoyer de l'information

Pour ce faire, tapez la commande `python3 process/clean-database.py`
Votre fichier d'élu.e.s est disponible ici : `/data/result/clean-representative-db.csv`


## Outil utilisé
Utilisation de scrapy https://docs.scrapy.org/ pour réaliser la récupération d'informations
