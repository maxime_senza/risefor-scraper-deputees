# This file is responsible for configuring your application
# and its dependencies with the aid of the Mix.Config module.
#
# This configuration file is loaded before any dependency and
# is restricted to this project.

# General application configuration
use Mix.Config

config :shop_scraper,
  ecto_repos: [ShopScraper.Repo]

# Configures the endpoint
config :shop_scraper, ShopScraperWeb.Endpoint,
  url: [host: "localhost"],
  secret_key_base: "x4JTYb0IBJY9Lz8zx8udylzXQ6rDCMyG2xVT5qq4aw/t7Te1sp3btV/BS37crNaB",
  render_errors: [view: ShopScraperWeb.ErrorView, accepts: ~w(html json)],
  pubsub: [name: ShopScraper.PubSub, adapter: Phoenix.PubSub.PG2]

# Configures Elixir's Logger
config :logger, :console,
  format: "$time $metadata[$level] $message\n",
  metadata: [:request_id]

# Use Jason for JSON parsing in Phoenix
config :phoenix, :json_library, Jason

# Import environment specific config. This must remain at the bottom
# of this file so it overrides the configuration defined above.
import_config "#{Mix.env()}.exs"
